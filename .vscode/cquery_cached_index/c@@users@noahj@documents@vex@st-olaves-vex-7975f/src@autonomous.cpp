#include "main.h"



/*
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */

// stuff we will actually use

#define LEFT_1 13
#define LEFT_2 14
#define RIGHT_1 11 //flip
#define RIGHT_2 12 //flip
#define SLIPGEAR 4
#define AIM 2
#define FLIPPER 10
#define INTAKE 3 //flip



#define SENSOR_0 'D'
#define SENSOR_1 'A'
#define SENSOR_2 'B'
#define SENSOR_SLIPGEAR 'C'

 void slipgear_fire() // slipgear fire function REMOVED POINTERS YAY!!
 {
   pros::Motor slpgr_fire(SLIPGEAR,true);
   slpgr_fire.tare_position();//zeros motor
   slpgr_fire.move_absolute(-2898,-200); //Moves 900 units which is 1 rotation with 18:1 gears
   while(slpgr_fire.get_position() > -2850)
   { //Waits until the slipgear has fully turned
     pros::delay(20);
     //printf("Slipgear Position :%f\n", slipgear.get_position());
   }
   pros::delay(200);
   //printf("Final slipgear position: %f\n",slipgear.get_position());
 }

 void aim(int distance) // slipgear adjustment function REMOVED POINTERS!!
 {
   pros::Motor slpgr_aim(AIM);
   slpgr_aim.tare_position();
   slpgr_aim.move_absolute(distance, 127);
   while(slpgr_aim.get_position() < (distance-50))
   {
     pros::delay(20);
   }
   pros::delay(20);
 }

 void turn(int distance)// Custom Turning function REMOVED POINTERS!!
 {
   pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
   pros::Motor left_mtr_2(LEFT_2);
 	 pros::Motor right_mtr_1(RIGHT_1,true);
 	 pros::Motor right_mtr_2(RIGHT_2,true);

   left_mtr_1.tare_position();//zeros ports
   left_mtr_2.tare_position();
   right_mtr_1.tare_position();
   right_mtr_2.tare_position();

   left_mtr_1.move_absolute(distance, 127);
   left_mtr_2.move_absolute(distance, 127);
   right_mtr_1.move_absolute((distance*(-1)), 127);
   right_mtr_2.move_absolute((distance*(-1)), 127);

   while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50)) &&
         (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50)) &&
         (right_mtr_1.get_position() < (distance - 50) || right_mtr_1.get_position() > (distance + 50)) &&
         (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
   {
     pros::delay(20);
   }
   pros::delay(20);
 }

 void drive(int distance)//straigt forward drive function
 {
   pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
   pros::Motor left_mtr_2(LEFT_2);
   pros::Motor right_mtr_1(RIGHT_1,true);
   pros::Motor right_mtr_2(RIGHT_2,true);

   left_mtr_1.move_absolute(distance, 200);
   right_mtr_1.move_absolute(distance, 200);
   right_mtr_2.move_absolute(distance, 200);
   left_mtr_2.move_absolute(distance, 200);
   while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50))&&
         (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50))&&
         (right_mtr_1.get_position() < (distance - 50)|| right_mtr_1.get_position() > (distance + 50))&&
         (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
   {
     pros::delay(20);
   }
   pros::delay(20);
   left_mtr_1.tare_position();
   right_mtr_1.tare_position();
   left_mtr_2.tare_position();
   right_mtr_2.tare_position();
 }

 void intake_on()
 {
   pros::Motor ball_intake(INTAKE,true);

   ball_intake.move(127);
 }

 void intake_auto_top()
 {
   pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
   pros::Motor ball_intake(INTAKE,true);

   while(sensor_top.get_value()>2000)
   {
     pros::delay(20);
   }
   ball_intake.move(0);
 }

 void intake_auto_hold()
 {
   pros::ADIAnalogIn sensor_1(SENSOR_1);
   pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
   pros::ADIAnalogIn sensor_2(SENSOR_2);
   pros::ADIAnalogIn sensor_0(SENSOR_0);
   pros::Motor ball_intake(INTAKE,true);

   while(((sensor_0.get_value()>2000))and((sensor_1.get_value()>2000))and((sensor_2.get_value()>2000)))
   {
     pros::delay(20);
   }
   ball_intake.move_voltage(-800);
 }

void autonomous() // holds all the programs
{

 //Port 2 vision sensor
  pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
	pros::Motor right_mtr_1(RIGHT_1,true);
	pros::Motor slpgr_fire(SLIPGEAR);
	pros::Motor flipper(FLIPPER);
	pros::Motor slpgr_aim(AIM);
	pros::Motor left_mtr_2(LEFT_2);
	pros::Motor right_mtr_2(RIGHT_2,true);
	pros::Motor ball_intake(INTAKE,true);

	pros::ADIAnalogIn sensor_1(SENSOR_1);
  pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
  pros::ADIAnalogIn sensor_2(SENSOR_2);
  pros::ADIAnalogIn sensor_0(SENSOR_0);

  flipper.tare_position();


  //auton selection booleans 3 variables = 8 options
  bool auton_blue = true; //Boolean that defines the side we are on
  bool back = false; //Boolean that defines what tile we are on
  bool option = true; //Boolean that defines what auton option we are running (false = option 0)

  // lists of autons
  if (auton_blue)
  {
    if(!back)
    {
      if(option) //Blue Front Option 1
      {
        flipper.move_relative(-400, -200);
        intake_on();
        drive(-2600);
        intake_auto_hold();
        drive(2600);
        pros::delay(500);
        turn(-800);
        pros::delay(500);
        slipgear_fire();
        //drive(1300);
        intake_on();
        pros::delay(750);
        slipgear_fire();
        pros::delay(500);
        drive(850);
        pros::delay(500);
        drive(850);
        pros::delay(500);
        drive(1000);
        pros::delay(500);
      }
      else //Blue Front Option 0
      {
        slipgear_fire(); //fire preload
        drive(2800); // drive forward 3.23 rotations (42 inch)
        pros::delay(1000);
        drive(-200); // drive backward 4 rotations (52 inch)
      /*  turn(424); // turn right (90deg 49 dia total 3.75 rotations 0.47 for 90 = 424)
        flipper.move_absolute(-400,-200); // moves flipper clear of intake
        pros::delay(250);
        intake_on(); // turns intake on
        drive(-2215); // drive backward 2.46 rotations (32 inch)
        intake_auto_off(); // waits for ball to be intaked and then off
        drive(2215); // drive forward 2.46 rotations (32 inch)
        turn(-424); // turn left (90deg 49 dia total 3.75 rotations 0.47 for 90 = 424)
        drive(1800); // drive forward 2 rotations (26 inch)
        slipgear_fire(); // shoot picked up ball
        */
      }
    }
    else
    {
      if(option) //Blue Back Option 1
      {

      }
      else //Blue Back Option 0
      {

      }
    }
  }
  else
  {
    if(!back)
    {
      if(option) //Red Front Option 1
      {
        flipper.move_relative(-400, -200);
        intake_on();
        drive(-2600);
        intake_auto_hold();
        drive(2800);
        pros::delay(500);
        turn(950);
        pros::delay(500);
        slipgear_fire();
        turn(-70);
        drive(1300);
        intake_on();
        pros::delay(750);
        turn(120);
        pros::delay(500);
        slipgear_fire();
        turn(-230);
        pros::delay(500);
        drive(1700);
        pros::delay(500);
        //drive(-1700);
        //drive(1000);
        //drive(-1000);


      }
      else //Red Front Option 0
      {
        slipgear_fire(); //fire preload
        turn(-100); // turn left (21 deg 0.11 rotations = 100)
        drive(2850); // drive forward 3.23 rotations (42 inch)
        pros::delay(1000);
        drive(-200); // drive backward 4 rotations (52 inch)
      /*  flipper.move_absolute(-600,-200); // moves flipper clear of intake
        turn(-424); // turn left (90deg 0.47 rotations = 424)
        intake_on(); // turns intake on
        drive(-2215); // drive backward 2.46 rotations (32 inch)
        intake_auto_off(); // waits for ball to be intaked and then off
        drive(2215); // drive forward 2.46 rotations (32 inch)
        turn(353); // turn right (75 deg 0.39 rotations = 353)
        drive(1800); // drive forward 2 rotations (26 inch)
        slipgear_fire(); // shoot picked up ball
        */
      }
    }
    else
    {
      if(option) //Red Back Option 1
      {

      }
      else //Red Back Option 0
      {
       pros::delay(1000);
      }
    }
  }
}
