#include "main.h"

#include <string>

#define LEFT_1 13
#define LEFT_2 14
#define RIGHT_1 11 //flip
#define RIGHT_2 12 //flip
#define SLIPGEAR 4
#define AIM 2 // flip
#define FLIPPER 10 // flip
#define INTAKE 3 //flip

#define VISION_SENSOR 5

#define SENSOR_0 'D'
#define SENSOR_1 'A'
#define SENSOR_2 'B'
#define SENSOR_SLIPGEAR 'C'



//By Joel

//TO DO
//Create a fire rate using Mutex
//Create vision sensor thing
bool ball_check(){
	pros::Vision ball_sensor(VISION_SENSOR);
	pros::vision_object_s_t BALL = ball_sensor.get_by_sig(0,1);
	if (BALL.width > 300){
		std::cout<<'.'<<std::endl;
		return true;
	}
	else{
		std::cout<<'!'<<std::endl;
		return false;
	}

}

bool check_sensor(int curr_val, int ambient) //Function for quickly testing if there is something in front of the line tracker
{
	return curr_val <=  2000;//(ambient -1000);
}

void intake_ball(void* x)
{
	pros::Vision ball_sensor(VISION_SENSOR);
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor intake(INTAKE, true);
	pros::ADIAnalogIn sensor_1(SENSOR_1);
	pros::ADIAnalogIn sensor_2(SENSOR_2);
	pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
  pros::ADIAnalogIn sensor_0(SENSOR_0);


  int sensor_ambient_0 = sensor_0.get_value();
	int sensor_ambient_1 = sensor_1.get_value(); //Right at the start, will measure the light, to account for ambient light
	int sensor_ambient_top = sensor_top.get_value();
	int sensor_ambient_2 = sensor_2.get_value();


	bool automatic_on = false; //Boolean for testing if the automatic mode is on
	//printf("Ambient sensor: %d\n", sensor_ambient);

	while(true) //Sets the loop
	{
    int current_sensor_0 = sensor_0.get_value();
		int current_sensor_1 = sensor_1.get_value(); //Measures the current light
		int current_sensor_2 = sensor_2.get_value();
		int current_sensor_top = sensor_top.get_value();
		if (master.get_digital(DIGITAL_LEFT)) //Measures for the new press of left
		{
			automatic_on = !automatic_on; //If there is a new press, will flip the boolean
			while(master.get_digital(DIGITAL_LEFT))
			{
				pros::delay(20);
			}
		}
		if(!automatic_on) //If the automatic is off
		{
			if (master.get_digital(DIGITAL_UP)) //If it is up, will move the intake
			{
				intake.move(127);
			}
			else if (master.get_digital(DIGITAL_DOWN)) //Else if Down is pressed  will move the intake down
			{
				intake.move(-127);
			}
			else
			{
				intake.move(0); //If neither, will stop the motor
			}
		}
		 else //If the automatic is on
		{
			if(((check_sensor(current_sensor_0, sensor_ambient_0)) or
					(check_sensor(current_sensor_1, sensor_ambient_1)) or
					(check_sensor(current_sensor_2, sensor_ambient_2))) and
					(ball_check())) //Checks if balls are in
			{
				 intake.move_voltage(-850); //will provide some resistance to stop the intake instantly
         pros::delay(20);
			}
			else //Otherwise will test for a button, which will load a ball
			{
				intake.move(127);
			}
		}
		//printf("Current Sensor: %d Ambient: %d \n", current_sensor_val,sensor_ambient);
		pros::delay(20); //Delay of 20ms
	}
}

void slipgear_fire(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor slipgear(SLIPGEAR, true);
	while(true)
	{
		if(master.get_digital_new_press(DIGITAL_L1)==1)
 		{ //If there is a new press of the L1 Button
			slipgear.tare_position();
 			slipgear.move_absolute(-2898,-200); //Moves 900 units which is 1 rotation with 18:1 gears
 			while(slipgear.get_position() > -2850)
 			{ //Waits until the slipgear has fully turned
 				pros::delay(20);
				//printf("Slipgear Position :%f\n", slipgear.get_position());
 			}
			slipgear.move(0);
			pros::delay(200);
			//printf("Final slipgear position: %f\n",slipgear.get_position());
	 	}
		pros::delay(20);
 	}
}

void aim(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor adjuster(AIM);
	while(true)
	{
		if (master.get_digital(DIGITAL_X)) // If the X button is pressed goes up
		{
			adjuster.move_velocity(80);

		} else if (master.get_digital(DIGITAL_B)) //If the B button is pressed goes down
		{
			adjuster.move_velocity(-80);

		} else
		{
			adjuster.move_velocity(0); //Else stops with PID hold
		}
	}
}

void slpgr_print_temp(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor slipgear(SLIPGEAR, true);
	int temp;
	int temp2;
	while(true)
	{ //Sets the loop
		temp2 = slipgear.get_temperature();
		if(temp2 != temp) //If the new temp hasnt already been printed, prevents preventable prints
		{
			temp =  temp2; //Sets the variable temp as the the variable before
			std::string smessage = "Temp: ";
			std::string stemp = std::to_string(temp); //Converts the temp to a string
			std::string sfull = smessage + stemp; //Creates a string with message and with
			char message[15]; //Sets a char array
			strcpy(message, sfull.c_str()); //Copies the string into an char array
			master.print(0,0,message); //Prints to the controller

		}
		pros::delay(1000); //Delays for 50ms which is refresh rate
	}

}

void drive(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor l_drive_1(LEFT_1);
	pros::Motor l_drive_2(LEFT_2);
	pros::Motor r_drive_1(RIGHT_1, true);
	pros::Motor r_drive_2(RIGHT_2, true);
	while (true)
	{
		int left = master.get_analog(ANALOG_LEFT_Y); //Sets the variables right and left as the controllers analog
		int right = master.get_analog(ANALOG_RIGHT_Y);
		l_drive_1.move(left); //Moves both the motors at what each analog wants
		l_drive_2.move(left);
		r_drive_1.move(right);
		r_drive_2.move(right);
		pros::delay(20); //Delays for 20ms
	}
}

void flipper_control(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor flipper_motor(FLIPPER);
  int	relative_pos;
	while (true)
	{
		if (master.get_digital_new_press(DIGITAL_L2))
		{
			relative_pos = flipper_motor.get_position();
			flipper_motor.move_relative(500, 200);
			while(flipper_motor.get_position()<(450+relative_pos))
			{
				pros::delay(20);
			}

			pros::delay(100);
			flipper_motor.move_relative(-500, -200);
			while(flipper_motor.get_position()>(50+relative_pos))
			{
				pros::delay(20);
			}
		}
		else if (master.get_digital(DIGITAL_R1)==1)
		{ //If R1 pressed make motor go up and then flipper
			flipper_motor.move_velocity(200);
		}
		else if (master.get_digital(DIGITAL_R2)==1)
		{ //If R2 pressed makes motor go down and then flipper
			flipper_motor.move_velocity(-200);
		}
		else if (master.get_digital(DIGITAL_Y)==1)
		{
			flipper_motor.move_absolute(-400, 200);
		}
		else if (master.get_digital(DIGITAL_A)==1)
		{
			flipper_motor.move_absolute(-3000, 200);
		}
		else if (master.get_digital(DIGITAL_RIGHT)==1)
		{
			flipper_motor.tare_position();
		}
		else
		{
			flipper_motor.move_relative(0, 40); //Else stops with PID hold
		}


			//printf("flipper Position: %lf\n", flipper_motor.get_position());
			pros::delay(20);
	}

}


/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {




  //pros::vision vision1 ( vex::PORT1, 50, BALL, SIG_2, SIG_3, SIG_4, SIG_5, SIG_6, SIG_7 );



	pros::Task slpgr_heat(slpgr_print_temp, 0, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT,"slpgr_print_temp");

	pros::Task drive_task(drive, 0, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "drive_task");

	pros::Task flipper_task(flipper_control, 0, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT, "flipper_task");

	pros::Task aim_task(aim, 0, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_MIN, "aim_task");

	pros::Task slpgr_shoot(slipgear_fire,0, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "slipgear_fire");

	pros::Task intake_task(intake_ball, 0, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT, "ball intake task");


	while (true)
	{

		pros::delay(20); //Delays to create refresh  rate
	}
}
