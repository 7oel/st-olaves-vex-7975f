#include "main.h"

#include <string>

#define DRIVE_NE 20 // flip
#define DRIVE_SE 18 // flip
#define DRIVE_SW 11
#define DRIVE_NW 12
#define LIFT_W 13
#define LIFT_E 19 // flip
#define ROLLERS 5
#define INTAKE 14



void drive(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor ne_drive(DRIVE_NE,true);
	pros::Motor se_drive(DRIVE_SE,true);
	pros::Motor sw_drive(DRIVE_SW);
	pros::Motor nw_drive(DRIVE_NW);
	pros::Motor e_lift(LIFT_E);

	while (true)
	{
		float limitMultiplier = 1;
		int ch1 = master.get_analog(ANALOG_RIGHT_X); //Sets the variables right and left as the controllers analog
		int ch2 = master.get_analog(ANALOG_RIGHT_Y);
		int ch3 = master.get_analog(ANALOG_LEFT_Y);
		int ch4 = master.get_analog(ANALOG_LEFT_X);
		nw_drive.move((ch3 + ch4 + ch1)*limitMultiplier); //Moves both the motors at what each analog wants
		ne_drive.move((ch2 - ch4 - ch1)*limitMultiplier);
		sw_drive.move((ch3 + ch4 - ch1)*limitMultiplier);
		se_drive.move((ch2 - ch4 + ch1)*limitMultiplier);
		pros::delay(20); //Delays for 20ms
	}
}

void flipper_control(void* x)
{
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	pros::Motor intake(INTAKE,true);
	pros::Motor e_lift(LIFT_E);
  pros::Motor w_lift(LIFT_W,true);
	while (true)
	{
		pros::delay(20);

		if (master.get_digital(DIGITAL_R1)){
			e_lift.move_absolute(0,200);
			w_lift.move_absolute(0,200);
			intake.move(127);
			while(e_lift.get_position()>30){
				pros::delay(20);
			}
			pros::delay(300);
			e_lift.move_absolute(300,200);
			w_lift.move_absolute(300,200);

			pros::delay(250);
			intake.move(5);
		}
		if (master.get_digital(DIGITAL_DOWN)){
			e_lift.move_absolute(150,200);
			w_lift.move_absolute(150,200);
		}
		else if (master.get_digital(DIGITAL_UP)){
			e_lift.move_absolute(1200,200);
			w_lift.move_absolute(1200,200);
		}
		else if (master.get_digital(DIGITAL_LEFT)){
			e_lift.move_absolute(600,200);
			w_lift.move_absolute(600,200);
		}
		else if (master.get_digital(DIGITAL_RIGHT)){
			e_lift.move_absolute(900,200);
			w_lift.move_absolute(900,200);
		}
		else if (master.get_digital(DIGITAL_R2)){
			e_lift.move(75);
			w_lift.move(75);
		}
		else if (master.get_digital(DIGITAL_L1)){
			e_lift.move(127);
			w_lift.move(127);
		}
		else if (master.get_digital(DIGITAL_L2)){
			e_lift.move(-127);
			w_lift.move(-127);
		}
		else {
			e_lift.move(0);
			w_lift.move(0);
		}

		if (master.get_digital(DIGITAL_R2)){
			intake.move(-110);
		}
		else if (master.get_digital(DIGITAL_X)){
			intake.move(127);
		}
		else if (master.get_digital(DIGITAL_B)){
			intake.move(-127);
		}
		else {
			intake.move(5);
		}
	}

}


/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {

	pros::Task drive_task(drive, 0, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "drive_task");

	pros::Task flipper_task(flipper_control, 0, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT, "flipper_task");

	{

		pros::delay(20); //Delays to create refresh  rate
	}
}
