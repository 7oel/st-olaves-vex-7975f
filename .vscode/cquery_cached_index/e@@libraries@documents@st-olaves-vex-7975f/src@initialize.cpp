#include "main.h"

void on_center_button() {
	static bool pressed = false;
	pressed = !pressed;
	if (pressed) {
		pros::lcd::set_text(2, "I was pressed!");
	} else {
		pros::lcd::clear_line(2);
	}
}

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
	/*
	pros::lcd::initialize();
	pros::lcd::set_text(1, "rip");

	pros::lcd::register_btn1_cb(on_center_button);
	//Port 2 vision sensor
  pros::Motor left_mtr_1(13); //Sets motors to their ports
 	pros::Motor right_mtr_1(11,true);
 	pros::Motor slpgr_fire(4);
 	pros::Motor flipper(10);
 	pros::Motor slpgr_aim(2);
 	pros::Motor left_mtr_2(14);
 	pros::Motor right_mtr_2(12,true);
 	pros::Motor ball_intake(3,true);

 	pros::ADIAnalogIn intake_top('A');
  pros::ADIAnalogIn slipgear_sensor('C');
  pros::ADIAnalogIn intake_second('B');


	left_mtr_1.tare_position();
	right_mtr_1.tare_position();
	slpgr_fire.tare_position();
	flipper.tare_position();
	slpgr_aim.tare_position();
	left_mtr_2.tare_position();
	right_mtr_2.tare_position();
	ball_intake.tare_position();*/
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {}
