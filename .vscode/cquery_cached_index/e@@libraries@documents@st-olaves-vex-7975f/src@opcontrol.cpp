#include "main.h"
#include <string>

#define LEFT_1 13
#define LEFT_2 14
#define RIGHT_1 11 //flip
#define RIGHT_2 12 //flip
#define SLIPGEAR 4
#define AIM 2
#define FLIPPER 10
#define INTAKE 3 //flip

#define SENSOR_1 'A'
#define SENSOR_2 'B'
#define SENSOR_SLIPGEAR 'C'



//By Joel

//TO DO
//Create a fire rate using Mutex
//Create vision sensor thing



bool check_sensor(int curr_val, int ambient) //Function for quickly testing if there is something in front of the line tracker
{
	return curr_val <= (ambient -1000);
}

void intake_ball(void* ball_struct_v)
{
	pros::Motor intake(INTAKE);
	pros::ADIAnalogIN sensor_1(SENSOR_1);
	pros::ADIAnalogIN sensor_2(SENSOR_2);
	pros::ADIAnalogIN sensor_top(SENSOR_SLIPGEAR);

	int sensor_ambient_1 = sensor_1.get_value(); //Right at the start, will measure the light, to account for ambient light
	int sensor_ambient_slipgear = sensor_top.get_value();
	int sensor_ambient_2 = sensor_1.get_value();
	bool automatic_on = false; //Boolean for testing if the automatic mode is on
	//printf("Ambient sensor: %d\n", sensor_ambient);

	while(true) //Sets the loop
	{
		int current_sensor_top = top_sensor.get_value(); //Measures the current light
		int current_sensor_second = second_sensor.get_value();
		int current_sensor_slipgear = slipgear_sensor.get_value();
		if (master_c.get_digital(DIGITAL_LEFT)) //Measures for the new press of left
		{
			automatic_on = !automatic_on; //If there is a new press, will flip the boolean
			while(master_c.get_digital(DIGITAL_LEFT))
			{
				pros::delay(20);
			}
		}
		if(!automatic_on) //If the automatic is off
		{
			if (master_c.get_digital(DIGITAL_UP)) //If it is up, will move the intake
			{
				intake.move(127);
			}
			else if (master_c.get_digital(DIGITAL_DOWN)) //Else if Down is pressed  will move the intake down
			{
				intake.move(-127);
			}
			else
			{
				intake.move(0); //If neither, will stop the motor
			}
		} else //If the automatic is on
		{
			if((!check_sensor(current_sensor_top, sensor_ambient_top) or
			!check_sensor(current_sensor_second, sensor_ambient_second)) and
			!check_sensor(current_sensor_slipgear, sensor_ambient_slipgear)) //Checks if ball isnt in
			{
				intake.move(127); //If it is it will turn on
			} else //Otherwise will test for a button, which will load a ball
			{
				intake.move(0);
			}
		}
		//printf("Current Sensor: %d Ambient: %d \n", current_sensor_val,sensor_ambient);
		pros::delay(20); //Delay of 20ms
	}
}

void slipgear_fire(void* fire_struct_v)
{

	while(true)
	{
		if(master_c.get_digital_new_press(DIGITAL_L1)==1)
 		{ //If there is a new press of the L1 Button
 			slipgear.move_absolute(2698,127); //Moves 900 units which is 1 rotation with 18:1 gears
 			while(slipgear.get_position() < 2650)
 			{ //Waits until the slipgear has fully turned
 				pros::delay(20);
				//printf("Slipgear Position :%f\n", slipgear.get_position());
 			}
			pros::delay(200);
			//printf("Final slipgear position: %f\n",slipgear.get_position());
			slipgear.tare_position();
	 	}
 	}
}

void aim(void* aim_struct_v)
{

	while(true)
	{
		if (master_c.get_digital(DIGITAL_X)) // If the X button is pressed goes up
		{
			aim_m.move(-127);

		} else if (master_c.get_digital(DIGITAL_B)) //If the B button is pressed goes down
		{
			aim_m.move(127);

		} else
		{
			aim_m.move(0); //Else stops
		}
	}
}


void slpgr_print_temp(void* motor_struct_v)
{

	int temp;
	int temp2;
	while(true)
	{ //Sets the loop
		temp2 = slpgr_f.get_temperature();
		if(temp2 != temp) //If the new temp hasnt already been printed, prevents preventable prints
		{
			temp =  temp2; //Sets the variable temp as the the variable before
			string smessage = "Temp: ";
			string stemp = to_string(temp); //Converts the temp to a string
			string sfull = smessage + stemp; //Creates a string with message and with
			char message[15]; //Sets a char array
			strcpy(message, sfull.c_str()); //Copies the string into an char array
			master_c.print(0,0,message); //Prints to the controller

		}
		pros::delay(1000); //Delays for 50ms which is refresh rate
	}

}

void drive(void* motor2_struct_v)
{
	while (true)
	{
		int left = master_c.get_analog(ANALOG_LEFT_Y); //Sets the variables right and left as the controllers analog
		int right = master_c.get_analog(ANALOG_RIGHT_Y);
		l_drive_1.move(left); //Moves both the motors at what each analog wants
		l_drive_2.move(left);
		r_drive_1.move(right);
		r_drive_2.move(right);
		pros::delay(20); //Delays for 20ms
	}
}


void flipper_control(void* flipper_struct_v)
{

	while (true)
	{
		if (master_c.get_digital_new_press(DIGITAL_L2))
		{
			flipper_motor.move_absolute(-400, 127);
			while(flipper_motor.get_position()>-190)
			{
				pros::delay(20);
			}
			flipper_motor.move(40);
			pros::delay(100);
		}
		else if (master_c.get_digital(DIGITAL_R1)==1)
		{ //If R1 pressed make motor go up and then flipper
			flipper_motor.move(-60);
		}
		else if (master_c.get_digital(DIGITAL_R2)==1)
		{ //If R2 pressed makes motor go down and then flipper
			flipper_motor.move(60);
		}
		else {
			flipper_motor.move(0); //Else stops
		}
			//printf("flipper Position: %lf\n", flipper_motor.get_position());
			pros::delay(20);
	}


}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
	//int fire_count = 0;

	pros::Controller master(pros::E_CONTROLLER_MASTER); //Sets the default Controller as master, as well as the motors for the left and right drive and the slipgear

	pros::Task slpgr_heat(slpgr_print_temp, NULL, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT,"slpgr_print_temp");

	pros::Task drive_task(drive, NULL, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "drive_task");

	pros::Task flipper_task(flipper_control, NULL, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT, "flipper_task");

	pros::Task aim_task(aim, NULL, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_MIN, "aim_task");

	pros::Task slpgr_shoot(slipgear_fire,NULL, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "slipgear_fire");

	pros::Task intake_task(intake_ball, NULL, TASK_PRIORITY_DEFAULT,TASK_STACK_DEPTH_DEFAULT, "ball intake task");


	while (true)
	{

		pros::lcd::print(0, "%d %d %d", (pros::lcd::read_buttons() & LCD_BTN_LEFT) >> 2,
		                 (pros::lcd::read_buttons() & LCD_BTN_CENTER) >> 1,
		                 (pros::lcd::read_buttons() & LCD_BTN_RIGHT) >> 0); //Came with code lol, cba to delete

		pros::delay(20); //Delays to create refresh  rate
		}

}
