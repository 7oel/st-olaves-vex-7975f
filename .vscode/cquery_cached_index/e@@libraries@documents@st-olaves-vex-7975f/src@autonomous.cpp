#include "main.h"

struct motor_sensor{
  pros::Motor* motor;
  pros::ADIAnalogIn* sensor;
  pros::Task* self;
};

bool check_sensor_auton(int curr_val, int ambient) //Function for quickly testing if there is something in front of the line tracker
{
	return curr_val <= (ambient -1000);
}

void dr4b_anti_drop(void* dr4b_v)
{
  pros::Motor dr4b = *(pros::Motor*)dr4b_v;
  while(true)
  {
    dr4b.move(-20);
    pros::delay(20);
  }
}

void intake_ball_auton(void* ball_struct_v)
{
  pros::delay(100);
  motor_sensor ball_struct = *(motor_sensor*)ball_struct_v;
  pros::Motor &intake = *(ball_struct.motor);
  pros::ADIAnalogIn &top_sensor = *(ball_struct.sensor);
  int ambient = top_sensor.get_value();
  int fire_count=0;
  while (true){
    if(!check_sensor_auton(top_sensor.get_value(), ambient))
    {
      intake.move(127);
    }
    else
    {

      intake.move(127);

      pros::delay(300);
      intake.move(0);
    }
    pros::delay(20);
  }
}

void slipgear_fire(pros::Motor* slipgear)
{
  slipgear->move_absolute(2698,127); //Moves 900 units which is 1 rotation with 18:1 gears
  while(slipgear->get_position() < 2680)
  { //Waits until the slipgear has fully turned
    pros::delay(20);
    //printf("Slipgear Position :%f\n", slipgear.get_position());
  }
  pros::delay(200);
  //printf("Final slipgear position: %f\n",slipgear.get_position());
  slipgear->tare_position();
}


void drive(int distance, pros::Motor* left_mtr_1, pros::Motor* left_mtr_2, pros::Motor* right_mtr_1, pros::Motor* right_mtr_2)
{
  left_mtr_1->move_absolute(distance, 127);
  right_mtr_1->move_absolute(distance, 127);
  right_mtr_2->move_absolute(distance, 127);
  left_mtr_2->move_absolute(distance, 127);
  while((left_mtr_1->get_position() < (distance - 50) || left_mtr_1->get_position() > (distance + 50))&&
        (left_mtr_2->get_position() < (distance - 50) || left_mtr_2->get_position() > (distance + 50))&&
        (right_mtr_1->get_position() < (distance - 50)|| right_mtr_1->get_position() > (distance + 50))&&
        (right_mtr_2->get_position() < (distance - 50) || right_mtr_2->get_position() > (distance + 50)))
  {
    pros::delay(20);
  }
  pros::delay(200);
  left_mtr_1 -> tare_position();
  right_mtr_1 -> tare_position();
  left_mtr_2 -> tare_position();
  right_mtr_2 -> tare_position();

}

void turn(int distance, pros::Motor* mtr_1, pros::Motor* mtr_2)
{
  mtr_1->move_absolute(distance, 127);
  mtr_2->move_absolute(distance, 127);
  while((mtr_1->get_position() < (distance - 50) || mtr_1->get_position() > (distance + 50)) &&
        (mtr_2->get_position() < (distance - 50) || mtr_2->get_position() > (distance + 50)))
  {
    pros::delay(20);
  }
  pros::delay(200);
  mtr_1 -> tare_position();
  mtr_2 -> tare_position();

}

void aim(int distance, pros::Motor* slpgr_aim)
{
  slpgr_aim->move_absolute(distance, 127);
  while(slpgr_aim->get_position() < (distance-50))
  {
    pros::delay(20);
  }
  slpgr_aim->tare_position();
  pros::delay(200);
}


/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {

  //Port 2 vision sensor
  pros::Motor left_mtr_1(9); //Sets motors to their ports
	pros::Motor right_mtr_1(14,true);
	pros::Motor slpgr_fire(7);
	pros::Motor dr4b(10);
	pros::Motor slpgr_aim(3);
	pros::Motor left_mtr_2(8);
	pros::Motor right_mtr_2(15,true);
	pros::Motor ball_intake(20,true);

	pros::ADIAnalogIn intake_top('A');

  motor_sensor intake_struct;
  intake_struct.motor = &ball_intake;
  intake_struct.sensor = &intake_top;
  pros::Task ball_intake_task(intake_ball_auton, (void*)&intake_struct, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT);
  pros::Task dr4b_adjust_task(dr4b_anti_drop, (void*)&dr4b, TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT);
  slpgr_aim.tare_position();
  pros::delay(500);
  bool auton_blue = true; //Boolean that defines the side we are on
  bool back = false;
  aim(1800, &slpgr_aim);
  slipgear_fire(&slpgr_fire); //Fires the slipgear
  if (auton_blue) //If we are on team blue
  {
      //blue
      //aim(100, &slpgr_aim); //Takes the aim and goes the highest it can
    if(!back){
      pros::delay(1000);
      //ball_intake_task.notify();
      drive(3150, &left_mtr_1,&left_mtr_2,&right_mtr_1,&right_mtr_2); //Drives forward
      pros::delay(1000); //Waits a second
      right_mtr_1.tare_position();
      right_mtr_2.tare_position();
      left_mtr_1.tare_position();
      left_mtr_2.tare_position();
      drive(-2700, &left_mtr_1,&left_mtr_2,&right_mtr_1,&right_mtr_2); //Drives backwards
      right_mtr_1.tare_position();
      right_mtr_2.tare_position();
      left_mtr_1.tare_position();
      left_mtr_2.tare_position();
      pros::delay(1000);
      turn(-1700, &right_mtr_1, &right_mtr_2);
      right_mtr_1.tare_position();
      right_mtr_2.tare_position();
      left_mtr_1.tare_position();
      left_mtr_2.tare_position();
  }
  } else
  {
    //red
    //aim(100, &slpgr_aim); //Takes the aim and goes the highest it can

    //ball_intake_task.notify();
    if(!back){
      pros::delay(1000);
      turn(300, &right_mtr_1, &right_mtr_2);
      pros::delay(500);
      drive(3500, &left_mtr_1,&left_mtr_2,&right_mtr_1,&right_mtr_2); //Drives forward
      pros::delay(1000); //Waits a second
      drive(-3500, &left_mtr_1,&left_mtr_2,&right_mtr_1,&right_mtr_2); //Drives backwards
      pros::delay(1000);
    }
    //turn(-1400,&left_mtr_1, &left_mtr_2);
    //drive(-3500, &left_mtr_1,&left_mtr_2,&right_mtr_1,&right_mtr_2);
    //drive(3500, &left_mtr_1,&left_mtr_2,&right_mtr_1,&right_mtr_2);
    //turn(1600, &left_mtr_1, &left_mtr_2);
    //slipgear_fire(&slpgr_fire);
    //ball_intake_task.notify();
  }
  delete &ball_intake_task;
  delete &dr4b_adjust_task;
}
