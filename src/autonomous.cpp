#include "main.h"



/*
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */

// stuff we will actually use

#define LEFT_1 13
#define LEFT_2 14
#define RIGHT_1 11 //flip
#define RIGHT_2 12 //flip
#define SLIPGEAR 4
#define AIM 2
#define FLIPPER 10
#define INTAKE 3 //flip



#define SENSOR_0 'D'
#define SENSOR_1 'A'
#define SENSOR_2 'B'
#define SENSOR_SLIPGEAR 'C'



void reverse_intake_on(){
   pros::Motor ball_intake(INTAKE,true);
   ball_intake.move(-127);
 }


void slipgear_fire()
 {
   pros::Motor slpgr_fire(SLIPGEAR,true); //sets the motor
   slpgr_fire.tare_position();//zeros motor
   slpgr_fire.move_absolute(-2900,-200); //Moves the units for a full rotation
   while(slpgr_fire.get_position() > -2850)
   { //Waits until the slipgear has fully turned
     pros::delay(20);
   }
   pros::delay(200);
 }

void aim(int preset)
 {
   pros::Motor adjuster(AIM);
   switch (preset){
     case 0:
     adjuster.move_absolute(0, 200);
     break;
     case 1:
     adjuster.move_absolute(550,200);
     break;
     case 2:
     adjuster.move_absolute(300,200);
     break;
     case 3:
     adjuster.move_absolute(580,200);
     break;
     default:
     adjuster.move_absolute(0, 200);
   }
 }

void turn(int distance)
 {
   pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
   pros::Motor left_mtr_2(LEFT_2);
 	 pros::Motor right_mtr_1(RIGHT_1,true);
 	 pros::Motor right_mtr_2(RIGHT_2,true);

   left_mtr_1.tare_position();//zeros ports
   left_mtr_2.tare_position();
   right_mtr_1.tare_position();
   right_mtr_2.tare_position();

   left_mtr_1.move_absolute(distance, 127); //Moves the left motors the distance
   left_mtr_2.move_absolute(distance, 127);
   right_mtr_1.move_absolute(-distance, 127); //Moves the right motors the reverse of the distance
   right_mtr_2.move_absolute(-distance, 127); //This is so that it goes the other way increasing the speed of the turn

   while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50)) &&
         (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50)) &&
         (right_mtr_1.get_position() < (distance - 50) || right_mtr_1.get_position() > (distance + 50)) &&
         (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
   { //Delays until motor reaches its target
     pros::delay(20);
   }
   pros::delay(20);
   left_mtr_1.tare_position();//zeros ports
   left_mtr_2.tare_position();
   right_mtr_1.tare_position();
   right_mtr_2.tare_position();
 }

 void slow_turn(int distance)
  {
    pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
    pros::Motor left_mtr_2(LEFT_2);
  	 pros::Motor right_mtr_1(RIGHT_1,true);
  	 pros::Motor right_mtr_2(RIGHT_2,true);

    left_mtr_1.tare_position();//zeros ports
    left_mtr_2.tare_position();
    right_mtr_1.tare_position();
    right_mtr_2.tare_position();

    left_mtr_1.move_absolute(distance, 100); //Moves the left motors the distance
    left_mtr_2.move_absolute(distance, 100);
    right_mtr_1.move_absolute(-distance, 100); //Moves the right motors the reverse of the distance
    right_mtr_2.move_absolute(-distance, 100); //This is so that it goes the other way increasing the speed of the turn

    while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50)) &&
          (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50)) &&
          (right_mtr_1.get_position() < (distance - 50) || right_mtr_1.get_position() > (distance + 50)) &&
          (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
    { //Delays until motor reaches its target
      pros::delay(20);
    }
    pros::delay(20);
    left_mtr_1.tare_position();//zeros ports
    left_mtr_2.tare_position();
    right_mtr_1.tare_position();
    right_mtr_2.tare_position();
  }
void center(int distance){
  pros::Motor left_mtr_1(LEFT_1);
  pros::Motor left_mtr_2(LEFT_2);
  pros::Motor right_mtr_1(RIGHT_1,true);
  pros::Motor right_mtr_2(RIGHT_2,true);

  left_mtr_1.move_absolute(distance, 200); //Sets the motors to move a certain distance
  right_mtr_1.move_absolute(distance, 200);
  right_mtr_2.move_absolute(distance, 200);
  left_mtr_2.move_absolute(distance, 200);
  while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50))&&
       (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50))&&
       (right_mtr_1.get_position() < (distance - 50)|| right_mtr_1.get_position() > (distance + 50))&&
       (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
  { //Continues to delay until motors have reached their positions
  pros::delay(20);
  }
  pros::delay(20);
}
void drive(int distance)//straigt forward drive function
 {

   pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
   pros::Motor left_mtr_2(LEFT_2);
   pros::Motor right_mtr_1(RIGHT_1,true);
   pros::Motor right_mtr_2(RIGHT_2,true);

   left_mtr_1.tare_position(); //Zeroes all the motors
   right_mtr_1.tare_position();
   left_mtr_2.tare_position();
   right_mtr_2.tare_position();

   left_mtr_1.move_absolute(distance, 200); //Sets the motors to move a certain distance
   right_mtr_1.move_absolute(distance, 200);
   right_mtr_2.move_absolute(distance, 200);
   left_mtr_2.move_absolute(distance, 200);
   while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50))&&
         (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50))&&
         (right_mtr_1.get_position() < (distance - 50)|| right_mtr_1.get_position() > (distance + 50))&&
         (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
   { //Continues to delay until motors have reached their positions
     pros::delay(20);
   }
   pros::delay(20);
   //left_mtr_1.tare_position();
   //right_mtr_1.tare_position();
   //left_mtr_2.tare_position();
   //right_mtr_2.tare_position(); //Zeroes all the motors again
 }

 void slow_drive(int distance)//straigt forward drive function
  {

    pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
    pros::Motor left_mtr_2(LEFT_2);
    pros::Motor right_mtr_1(RIGHT_1,true);
    pros::Motor right_mtr_2(RIGHT_2,true);

    left_mtr_1.tare_position(); //Zeroes all the motors
    right_mtr_1.tare_position();
    left_mtr_2.tare_position();
    right_mtr_2.tare_position();

    left_mtr_1.move_absolute(distance, 127); //Sets the motors to move a certain distance
    right_mtr_1.move_absolute(distance, 127);
    right_mtr_2.move_absolute(distance, 127);
    left_mtr_2.move_absolute(distance, 127);
    while((left_mtr_1.get_position() < (distance - 50) || left_mtr_1.get_position() > (distance + 50))&&
          (left_mtr_2.get_position() < (distance - 50) || left_mtr_2.get_position() > (distance + 50))&&
          (right_mtr_1.get_position() < (distance - 50)|| right_mtr_1.get_position() > (distance + 50))&&
          (right_mtr_2.get_position() < (distance - 50) || right_mtr_2.get_position() > (distance + 50)))
    { //Continues to delay until motors have reached their positions
      pros::delay(20);
    }
    pros::delay(20);
    //left_mtr_1.tare_position();
    //right_mtr_1.tare_position();
    //left_mtr_2.tare_position();
    //right_mtr_2.tare_position(); //Zeroes all the motors again
  }

void intake_on()//Turns the intake on.
 {
   pros::Motor ball_intake(INTAKE,true);
   ball_intake.move(127);
 }

void intake_auto_top()
 {
   pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
   pros::Motor ball_intake(INTAKE,true);

   while(sensor_top.get_value()>2000) //Intakes until the sensor on the slipgear detects a ball
   {
     pros::delay(20);
   }
   ball_intake.move(0);
 }

void intake_auto_hold()
 {
   pros::ADIAnalogIn sensor_1(SENSOR_1);
   pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
   pros::ADIAnalogIn sensor_2(SENSOR_2);
   pros::ADIAnalogIn sensor_0(SENSOR_0);
   pros::Motor ball_intake(INTAKE,true);

   while(((sensor_0.get_value()>2000))and((sensor_1.get_value()>2000))and((sensor_2.get_value()>2000)))
   { //If None of the sensors detect a ball
     pros::delay(20);
   }
   ball_intake.move_voltage(-800);
 }

void autonomous() // holds all the programs
{

 //Port 2 vision sensor
  pros::Motor left_mtr_1(LEFT_1); //Sets motors to their ports
	pros::Motor right_mtr_1(RIGHT_1,true);
	pros::Motor slpgr_fire(SLIPGEAR);
	pros::Motor flipper(FLIPPER);
	pros::Motor slpgr_aim(AIM);
	pros::Motor left_mtr_2(LEFT_2);
	pros::Motor right_mtr_2(RIGHT_2,true);
	pros::Motor ball_intake(INTAKE,true);

	pros::ADIAnalogIn sensor_1(SENSOR_1); //Sets sensor to their ports
  pros::ADIAnalogIn sensor_top(SENSOR_SLIPGEAR);
  pros::ADIAnalogIn sensor_2(SENSOR_2);
  pros::ADIAnalogIn sensor_0(SENSOR_0);

  flipper.tare_position(); //Zeroes flipper


  //auton selection booleans 3 variables = 8 options
  bool auton_blue = true; //Boolean that defines the side we are on
  bool back = false; //Boolean that defines what tile we are on
  bool option = true; //Boolean that defines what auton option we are running (false = option 0)

  // lists of autons
  if (auton_blue)
  {
    if(!back)
    {
      if(option) //Blue Front Option 1
      {
        flipper.move_relative(-300,-200); //Moves the flipper up to avoid intefering with intake
        pros::delay(250);
        intake_on();//turns intake on
        slow_drive(-2600); //Drives backward to pick up ball
        intake_auto_hold(); //Holds the auton until the ball is picked up
        center(-2600);
        pros::delay(400);
        slow_drive(2750); //Drives forward to go back to flag
        pros::delay(400);
        slow_turn(-790); //turns to face flag
        pros::delay(400);
        slow_drive(300);
        pros::delay(400);
        slipgear_fire(); //fires slipgear
        pros::delay(200);
        intake_on();
        aim(1); //Intakes ball and changes the aim to hit middle flag
        intake_auto_top();//Intakes ball to the top
        pros::delay(200);
        slipgear_fire(); //fires to hit middle
        slow_drive(3300); //drives forward to hit bottom flag
        pros::delay(400);
        aim(0); //Intakes ball and changes the aim to hit middle flag
        slow_drive(-1750);//drive backward to flip flag
        pros::delay(300);
        turn(900); //Turns 90 degrees to hit cap

        flipper.move_relative(-100,-200); //Moves the flipper up to avoid intefering with intake
        reverse_intake_on();
        pros::delay(500);
        drive(-1800);

      }
      else //Blue Front Option 0
      {
        flipper.move_relative(-200, -200); //Moves the flipper up to avoid intefering with intake
        intake_on(); ///turns intake on
        drive(-2600); //Drives backward to pick up ball
        intake_auto_hold(); //Holds the auton until the ball is picked up
        drive(2550);//Holds the auton until the ball is picked up
        pros::delay(500);
        turn(-750); //turns to face flag
        pros::delay(500);
        slipgear_fire();//fires slipgear
        pros::delay(1000);
        drive(1500); //drives forward
        intake_on(); //turns intake to place ball in slipgear
        pros::delay(850);
        slipgear_fire(); //fires again
        pros::delay(500);
        turn(-50);
        drive(1600);
        pros::delay(500);
        drive(-2000);
        pros::delay(500);
        turn(900);
        pros::delay(500);
        ball_intake.move(-127);
        drive(-2000);
      }
    }
    else
    {
      if(option) //Blue Back Option 1
      {
        flipper.move_relative(-300, -200);
        intake_on();
        slow_drive(-2600);
        intake_auto_hold();
        pros::delay(500);
        aim(2);
        drive(150);
        pros::delay(500);
        slow_turn(-1060);//900 for midfdle
        pros::delay(2500);
        slipgear_fire();
        pros::delay(500);
        intake_on();
        aim(3);
        pros::delay(1000);
        slipgear_fire();
        pros::delay(500);
        slow_turn(230);
        pros::delay(500);
        aim(0);
        slow_drive(2650);
      }
      else //Blue Back Option 0
      {
        flipper.move_relative(-400, -200);
        intake_on();
        drive(-2600);
        intake_auto_hold();
        drive(600);
        pros::delay(750);
        turn(-830);
        drive(2000);
      }
    }
  }
  else
  {
    if(!back)
    {
      if(option) //Red Front Option 1 (skills)
      {
        flipper.move_relative(-300,-200); //Moves the flipper up to avoid intefering with intake
        pros::delay(250);
        intake_on();//turns intake on
        slow_drive(-2600); //Drives backward to pick up ball
        intake_auto_hold(); //Holds the auton until the ball is picked up
        center(-2600);
        slow_drive(2900); //Drives forward to go back to flag
        pros::delay(400);
        slow_turn(1040); //turns to face flag
        pros::delay(400);
        slow_drive(350);
        pros::delay(400);
        slipgear_fire(); //fires slipgear
        pros::delay(200);
        intake_on();
        aim(1); //Intakes ball and changes the aim to hit middle flag
        intake_auto_top();//Intakes ball to the top
        pros::delay(700);
        slipgear_fire(); //fires to hit middle
        flipper.move_absolute(0, 200);
        pros::delay(400);
        slow_turn(-140);
        slow_drive(3300);
        aim(0);
        pros::delay(400);
        slow_drive(-5000);//drive backward to flip flag
        pros::delay(500);
        slow_turn(900);
        pros::delay(500);
        pros::delay(500);
        slow_drive(6100);



      }
      else //Red Front Option 0
      {
        flipper.move_relative(-300,-200); //Moves the flipper up to avoid intefering with intake
        pros::delay(250);
        intake_on();//turns intake on
        drive(-2600); //Drives backward to pick up ball
        intake_auto_hold(); //Holds the auton until the ball is picked up
        center(-2600);
        drive(2800); //Drives forward to go back to flag
        pros::delay(400);
        turn(1050); //turns to face flag
        pros::delay(400);
        drive(350);
        pros::delay(400);
        slipgear_fire(); //fires slipgear
        pros::delay(200);
        intake_on();
        aim(1); //Intakes ball and changes the aim to hit middle flag
        intake_auto_top();//Intakes ball to the top
        pros::delay(200);
        slipgear_fire(); //fires to hit middle
        pros::delay(400);
        turn(-110);
        drive(3300);
        aim(0);
        pros::delay(400);
        drive(-1750);//drive backward to flip flag
        pros::delay(300);
        turn(-900); //Turns 90 degrees to hit cap
        reverse_intake_on();
        pros::delay(400);
        drive(-1800);



      }
    }
    else
    {
      if(option) //Red Back Option 1 1 cap, 3 flag
      {
        flipper.move_relative(-300, -200);
        intake_on();
        slow_drive(-2600);
        intake_auto_hold();
        pros::delay(500);
        aim(2);
        drive(150);
        pros::delay(500);
        slow_turn(1180);//900 for midfdle
        pros::delay(3000);
        slipgear_fire();
        pros::delay(500);
        intake_on();
        aim(3);
        pros::delay(1000);
        slipgear_fire();
        pros::delay(500);
        slow_turn(-350);
        pros::delay(500);
        aim(0);
        slow_drive(2650);
      }
      else //Red Back Option 0 parking
      {
        flipper.move_relative(-300, -200);
        intake_on();
        drive(-2600);
        intake_auto_hold();
        drive(250);
        pros::delay(750);
        turn(830);
        drive(2830);
      }
    }
  }
}
